import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { CambiosPage } from '../cambios.page';

@Component({
  selector: 'app-cambiosform',
  templateUrl: './cambiosform.page.html',
  styleUrls: ['./cambiosform.page.scss'],
})
export class CambiosformPage implements OnInit {
  form: FormGroup;
  constructor(private Cambios:CambiosPage, private router:Router) { }

  ngOnInit() {

    this.form = new FormGroup({
      titulo: new FormControl(null, {
        updateOn: 'blur', validators: [Validators.required]
      }),
      descripcion: new FormControl(null, {
        updateOn: 'blur', validators:[Validators.required, Validators.maxLength(180)]
      }),
      precio: new FormControl(null, {
        updateOn: 'blur', validators: [Validators.required, Validators.min(1)]
      }),
    });

  }

  onCrearCambio(){
    if(!this.form.valid){
      return;
    }
    this.Cambios.addCambio(this.form.value.titulo, this.form.value.descripcion, +this.form.value.precio);
    this.form.reset();
    this.router.navigate(['lugares/tabs/ofertas']);

  }


}
