import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CambiosformPage } from './cambiosform.page';

const routes: Routes = [
  {
    path: '',
    component: CambiosformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CambiosformPageRoutingModule {}
