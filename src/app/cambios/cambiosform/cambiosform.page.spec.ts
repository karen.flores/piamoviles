import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CambiosformPage } from './cambiosform.page';

describe('CambiosformPage', () => {
  let component: CambiosformPage;
  let fixture: ComponentFixture<CambiosformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambiosformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CambiosformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
