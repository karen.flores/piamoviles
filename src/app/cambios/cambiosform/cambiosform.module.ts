import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CambiosformPageRoutingModule } from './cambiosform-routing.module';

import { CambiosformPage } from './cambiosform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CambiosformPageRoutingModule
  ],
  declarations: [CambiosformPage]
})
export class CambiosformPageModule {}
