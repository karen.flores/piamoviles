import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-cambios',
  templateUrl: './cambios.page.html',
  styleUrls: ['./cambios.page.scss'],
})
export class CambiosPage implements OnInit {

  constructor(private alertCtrl: AlertController, private loadingController: LoadingController, private router: Router) { }


  presentConfirm() {
    let alert = this.alertCtrl.create({
      header: '¿Seguro de la compra?', 
      buttons: [
        {text: 'Si', handler:()=>{
          this.handleButtonClick()
        }},
        {text: 'Cancelar', role:'cancel'}
      ]
    })
    .then(alertCtrl=>{
      alertCtrl.present();
    });
  }

  handleButtonClick() {
    const loading = this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    })
    .then(loadingController=>{
      loadingController.present();
    });
    this.success()
  }

  success(){
    let alert = this.alertCtrl.create({
      header: 'Solicitud Exitosa', 
      buttons: [
        {text: 'Ok', handler:()=>{
          this.router.navigate(['/','carros', 'catalogo'])
        }},
      ]
    })
    .then(alertCtrl=>{
      alertCtrl.present();
    });
  }
  
  ngOnInit() {

  }

}
