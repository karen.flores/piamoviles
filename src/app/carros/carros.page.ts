import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-carros',
  templateUrl: './carros.page.html',
  styleUrls: ['./carros.page.scss'],
})
export class CarrosPage implements OnInit {

  constructor(private router: Router) { }

  continue(){
    this.router.navigate(['/', 'carros', 'catalogo'])
  }
  ngOnInit() {
  }

}
