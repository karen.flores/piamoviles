import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {

  constructor(private alertCtrl: AlertController, private loadingController: LoadingController, private router: Router) { }


  handleButtonClick() {
    const loading = this.loadingController.create({
      message: 'Please wait...',
      duration: 2000
    })
    .then(loadingController=>{
      loadingController.present();
    });
    this.router.navigate(['/','carros', 'catalogo']),
    this.success()
  }

  success(){
    let alert = this.alertCtrl.create({
      header: 'Compra Exitosa', 
      buttons: [
        {text: 'Ok', handler:()=>{
        }},
      ]
    })
    .then(alertCtrl=>{
      alertCtrl.present();
    });
  }
  

  ngOnInit() {
  }

}