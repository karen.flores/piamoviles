import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.page.html',
  styleUrls: ['./catalogo.page.scss'],
})
export class CatalogoPage implements OnInit {
  items:any;
  carros = [

    {
      direccion: 'camry',
      inicial: 'camry',
      nombre: 'Camry Hybrid 2020',
      precio: '227990',
      descripcion: 'Trendline',
      imagen: 'assets/carros/camry.png'
    },
    {
      direccion: 'corolla',
      inicial: 'corolla',
      nombre: 'Corolla 2020',
      precio: '341700',
      descripcion: 'Startline',
      imagen: 'assets/carros/corolla.png'
    },
    {
      direccion: 'jetta',
      inicial: 'jetta',
      nombre: 'Jetta 2020',
      precio: '227990',
      descripcion: 'Trendline',
      imagen: 'assets/carros/camry.png'
    },
    {
      direccion: 'polo',
      inicial: 'polo',
      nombre: 'Polo 2020',
      precio: '228990',
      descripcion: 'Trendline',
      imagen: 'assets/carros/polo.png'
    },
    {
      direccion: 'tiguan',
      inicial: 'tiguan',
      nombre: 'Tiguan 2020',
      precio: '227990',
      descripcion: 'Startline',
      imagen: 'assets/carros/tiguan.png'
    },
    {
      direccion: 'vitrus',
      inicial: 'v',
      nombre: 'Vitrus 2020',
      precio: '295990',
      descripcion: 'Trendline',
      imagen: 'assets/carros/virtus.png'
    },
    {
      direccion: 'yaris',
      inicial: 'y',
      nombre: 'Yaris Sedán 2020',
      precio: '341700',
      descripcion: 'TStartline',
      imagen: 'assets/carros/yaris.png'
    }

  ]
  

  constructor() { 
    this.initializeItems();
  }

  
  ngOnInit() {
  }

  initializeItems() {
    this.items= this.carros
   }
   getItems(ev:any) {
    this.initializeItems();
  
    let val = ev.target.value;
    if(val && val.trim() != '') {
    this.items = this.items.filter((item) => {
     return (item.inicial.toLowerCase().indexOf(val.toLowerCase()) > -1);
   })
   }
   }


}
