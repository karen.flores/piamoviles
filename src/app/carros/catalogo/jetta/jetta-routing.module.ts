import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JettaPage } from './jetta.page';

const routes: Routes = [
  {
    path: '',
    component: JettaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JettaPageRoutingModule {}
