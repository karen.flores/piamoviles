import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JettaPage } from './jetta.page';

describe('JettaPage', () => {
  let component: JettaPage;
  let fixture: ComponentFixture<JettaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JettaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JettaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
