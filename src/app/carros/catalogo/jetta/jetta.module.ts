import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JettaPageRoutingModule } from './jetta-routing.module';

import { JettaPage } from './jetta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JettaPageRoutingModule
  ],
  declarations: [JettaPage]
})
export class JettaPageModule {}
