import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorollaPage } from './corolla.page';

describe('CorollaPage', () => {
  let component: CorollaPage;
  let fixture: ComponentFixture<CorollaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorollaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorollaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
