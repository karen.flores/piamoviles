import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TiguanPage } from './tiguan.page';

describe('TiguanPage', () => {
  let component: TiguanPage;
  let fixture: ComponentFixture<TiguanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiguanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TiguanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
