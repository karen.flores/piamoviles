import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiguanPage } from './tiguan.page';

const routes: Routes = [
  {
    path: '',
    component: TiguanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TiguanPageRoutingModule {}
