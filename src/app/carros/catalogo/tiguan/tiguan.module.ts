import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TiguanPageRoutingModule } from './tiguan-routing.module';

import { TiguanPage } from './tiguan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TiguanPageRoutingModule
  ],
  declarations: [TiguanPage]
})
export class TiguanPageModule {}
