import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogoPage } from './catalogo.page';

const routes: Routes = [
  {
    path: '',
    component: CatalogoPage
  },
  {
    path: 'camry',
    loadChildren: () => import('./camry/camry.module').then( m => m.CamryPageModule)
  },
  {
    path: 'jetta',
    loadChildren: () => import('./jetta/jetta.module').then( m => m.JettaPageModule)
  },
  {
    path: 'polo',
    loadChildren: () => import('./polo/polo.module').then( m => m.PoloPageModule)
  },
  {
    path: 'tiguan',
    loadChildren: () => import('./tiguan/tiguan.module').then( m => m.TiguanPageModule)
  },
  {
    path: 'vitrus',
    loadChildren: () => import('./vitrus/vitrus.module').then( m => m.VitrusPageModule)
  },
  {
    path: 'yaris',
    loadChildren: () => import('./yaris/yaris.module').then( m => m.YarisPageModule)
  },
  {
    path: 'corolla',
    loadChildren: () => import('./corolla/corolla.module').then( m => m.CorollaPageModule)
  },
  {
    path: 'ubicacion',
    loadChildren: () => import('./ubicacion/ubicacion.module').then( m => m.UbicacionPageModule)
  },
  {
    path: 'compras',
    loadChildren: () => import('./compras/compras.module').then( m => m.ComprasPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogoPageRoutingModule {}
