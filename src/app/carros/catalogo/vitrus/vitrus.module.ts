import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VitrusPageRoutingModule } from './vitrus-routing.module';

import { VitrusPage } from './vitrus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VitrusPageRoutingModule
  ],
  declarations: [VitrusPage]
})
export class VitrusPageModule {}
