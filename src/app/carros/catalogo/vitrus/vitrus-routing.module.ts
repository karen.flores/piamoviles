import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VitrusPage } from './vitrus.page';

const routes: Routes = [
  {
    path: '',
    component: VitrusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VitrusPageRoutingModule {}
