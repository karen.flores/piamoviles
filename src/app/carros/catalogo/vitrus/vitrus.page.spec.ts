import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VitrusPage } from './vitrus.page';

describe('VitrusPage', () => {
  let component: VitrusPage;
  let fixture: ComponentFixture<VitrusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitrusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VitrusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
