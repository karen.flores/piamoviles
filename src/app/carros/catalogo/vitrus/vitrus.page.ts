import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-vitrus',
  templateUrl: './vitrus.page.html',
  styleUrls: ['./vitrus.page.scss'],
})
export class VitrusPage implements OnInit {

  constructor(private alertCtrl: AlertController, private router: Router) { }


  presentConfirm() {
    let alert = this.alertCtrl.create({
      header: '¿Seguro de la compra?', 
      buttons: [
        {text: 'Si', handler:()=>{
          this.router.navigate(['/', 'carros', 'catalogo', 'compras'])
        }},
        {text: 'Cancelar', role:'cancel'}
      ]
    })
    .then(alertCtrl=>{
      alertCtrl.present();
    });
  }

  ngOnInit() {
  }

}
