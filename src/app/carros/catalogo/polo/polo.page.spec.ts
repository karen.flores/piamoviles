import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PoloPage } from './polo.page';

describe('PoloPage', () => {
  let component: PoloPage;
  let fixture: ComponentFixture<PoloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoloPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PoloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
