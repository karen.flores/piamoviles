import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PoloPage } from './polo.page';

const routes: Routes = [
  {
    path: '',
    component: PoloPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PoloPageRoutingModule {}
