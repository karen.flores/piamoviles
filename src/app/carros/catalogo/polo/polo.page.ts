import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-polo',
  templateUrl: './polo.page.html',
  styleUrls: ['./polo.page.scss'],
})
export class PoloPage implements OnInit {

  constructor(private alertCtrl: AlertController, private router: Router) { }


  presentConfirm() {
    let alert = this.alertCtrl.create({
      header: '¿Seguro de la compra?', 
      buttons: [
        {text: 'Si', handler:()=>{
          this.router.navigate(['/', 'carros' ,'catalogo', 'compras'])
        }},
        {text: 'Cancelar', role:'cancel'}
      ]
    })
    .then(alertCtrl=>{
      alertCtrl.present();
    });
  }

  ngOnInit() {
  }

}
