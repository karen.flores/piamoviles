import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PoloPageRoutingModule } from './polo-routing.module';

import { PoloPage } from './polo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PoloPageRoutingModule
  ],
  declarations: [PoloPage]
})
export class PoloPageModule {}
