import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YarisPage } from './yaris.page';

const routes: Routes = [
  {
    path: '',
    component: YarisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class YarisPageRoutingModule {}
