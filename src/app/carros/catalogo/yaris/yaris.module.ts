import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { YarisPageRoutingModule } from './yaris-routing.module';

import { YarisPage } from './yaris.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    YarisPageRoutingModule
  ],
  declarations: [YarisPage]
})
export class YarisPageModule {}
