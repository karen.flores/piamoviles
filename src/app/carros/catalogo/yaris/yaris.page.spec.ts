import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { YarisPage } from './yaris.page';

describe('YarisPage', () => {
  let component: YarisPage;
  let fixture: ComponentFixture<YarisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YarisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(YarisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
