import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CamryPage } from './camry.page';

const routes: Routes = [
  {
    path: '',
    component: CamryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CamryPageRoutingModule {}
