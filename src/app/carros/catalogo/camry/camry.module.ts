import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CamryPageRoutingModule } from './camry-routing.module';

import { CamryPage } from './camry.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CamryPageRoutingModule
  ],
  declarations: [CamryPage]
})
export class CamryPageModule {}
