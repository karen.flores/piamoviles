import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CamryPage } from './camry.page';

describe('CamryPage', () => {
  let component: CamryPage;
  let fixture: ComponentFixture<CamryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CamryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
