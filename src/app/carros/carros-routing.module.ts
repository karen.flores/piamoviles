import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarrosPage } from './carros.page';


const routes: Routes = [
      {   
        path: "catalogo",

            loadChildren: () =>
            import('./catalogo/catalogo.module').then(
              (m) => m.CatalogoPageModule),
      },
     { path: "",
    redirectTo: "/carros/catalogo",
    pathMatch: "full",
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarrosPageRoutingModule {}
